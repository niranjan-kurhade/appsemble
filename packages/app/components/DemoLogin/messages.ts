import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  cancel: {
    id: 'app.47FYwb',
    defaultMessage: 'Cancel',
  },
  login: {
    id: 'app.AyGauy',
    defaultMessage: 'Login',
  },
  loginFailed: {
    id: 'app.nuWwea',
    defaultMessage: 'Login failed',
  },
  selectRole: {
    id: 'app.+i6kfV',
    defaultMessage: 'Select a role',
  },
  selectTeamRole: {
    id: 'app.obEU1l',
    defaultMessage: 'Select a team role',
  },
  member: {
    id: 'app.7L86Z5',
    defaultMessage: 'Member',
  },
  manager: {
    id: 'app.KQzcqr',
    defaultMessage: 'Manager',
  },
  joinTeam: {
    id: 'app.C5l8n7',
    defaultMessage: 'Join team',
  },
  leaveTeam: {
    id: 'app.sPEM/n',
    defaultMessage: 'Leave team',
  },
  error: {
    id: 'app.ox304v',
    defaultMessage: 'An error occurred',
  },
});
