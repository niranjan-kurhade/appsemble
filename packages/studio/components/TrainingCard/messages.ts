import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  linkToDocumentation: {
    id: 'studio.oZ5TXL',
    defaultMessage: 'Link to documentation',
  },
  video: {
    id: 'studio.psLpik',
    defaultMessage: 'Instruction Video',
  },
  exampleCode: {
    id: 'studio.dOtaub',
    defaultMessage: 'Example code',
  },
  externalResource: {
    id: 'studio.jSQuj+',
    defaultMessage: 'External Resource',
  },
});
