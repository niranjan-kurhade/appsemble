import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  competence: {
    id: 'studio.fs5Fko',
    defaultMessage: 'Competence',
  },
  difficulty: {
    id: 'studio.aWgR+h',
    defaultMessage: 'Difficulty level',
  },
});
