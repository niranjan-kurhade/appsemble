import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: {
    id: 'studio.n7yYXG',
    defaultMessage: 'Service',
  },
  error: {
    id: 'studio.FL26DE',
    defaultMessage: 'There was a problem loading the service secrets.',
  },
  loading: {
    id: 'studio.meTsdt',
    defaultMessage: 'Loading service secrets …',
  },
  placeholder: {
    id: 'studio.AgUG6b',
    defaultMessage: 'example.com',
  },
  noSecrets: {
    id: 'studio.TAMTr1',
    defaultMessage: 'No service secrets have been configured yet.',
  },
  addNew: {
    id: 'studio.v9WChN',
    defaultMessage: 'Add new secret',
  },
});
