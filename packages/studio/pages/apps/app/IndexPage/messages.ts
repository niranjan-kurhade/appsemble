import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  description: {
    id: 'studio.Q8Qw5B',
    defaultMessage: 'Description',
  },
  view: {
    id: 'studio.Kn7A8C',
    defaultMessage: 'View App',
  },
  readMore: {
    id: 'studio.S++WdB',
    defaultMessage: 'Read more',
  },
  readLess: {
    id: 'studio.Ed2ywJ',
    defaultMessage: 'Read less',
  },
  exportText: {
    id: 'studio.SVwJTM',
    defaultMessage: 'Export',
  },
  exportWithResources: {
    id: 'studio.ny2yoG',
    defaultMessage: 'Include resources in the export file.',
  },
});
